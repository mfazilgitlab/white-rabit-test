import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LocationModel, PictureModel, UserModel } from 'src/app/models/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  @Output() usersEmitter = new EventEmitter<UserModel[]>();
  userModel: UserModel = { name: {}, picture: new PictureModel(), location: new LocationModel() };
  constructor() { }

  ngOnInit(): void {
  }
  addUser() {
    console.log(this.userModel);
    let userData = JSON.parse(localStorage.getItem("users"))
    userData.unshift({ "user": this.userModel });
    localStorage.setItem("users", JSON.stringify(userData));
    this.usersEmitter.emit(userData);
  }
}
