import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userData: any = [];
  searchKey: string = "";
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUserList().subscribe((data) => {
      this.userData = data["results"];
      localStorage.setItem("users", JSON.stringify(this.userData));
    },
      error => {
      });

  }
  addUser(data) {
    this.userData = data;
  }
  search() {
    let data = JSON.parse(localStorage.getItem("users"));
    this.userData = this.checkExist(data, this.searchKey);
  }
  checkExist(datas, email) {
    let filtered=[];
    datas.forEach(element => {
      if(element["user"].email==email){
        filtered.push(element);
      }
    });
   return filtered;
  }
  clear() {
    this.searchKey = "";
    this.userData = JSON.parse(localStorage.getItem("users"))
  }
}
