import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home.component';
import { AddUserComponent } from './add-user/add-user.component';


@NgModule({
  declarations: [UserComponent, HomeComponent, AddUserComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule
  ]
})
export class HomeModule { }
