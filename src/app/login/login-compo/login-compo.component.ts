import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/models/login';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-compo',
  templateUrl: './login-compo.component.html',
  styleUrls: ['./login-compo.component.css'],
  providers: [LoginService]
})
export class LoginCompoComponent implements OnInit {
  loginModel: LoginModel = new LoginModel();
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }
  login() {
    this.loginService.getLoginList().subscribe((data) => {
      let user = data.filter(it => it["username"] == this.loginModel.userName && it["password"] == this.loginModel.password);
      if (user) {
        this.router.navigate(['home']);
      } else {

      }
    },
      error => {
      });

  }
}
