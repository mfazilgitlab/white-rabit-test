export class UserModel {
    userName?: string
    password?: string
    dob?: string
    phone?: string
    email?: string
    name?: NameModel
    gender?: string = "male";
    location?: LocationModel
    picture?: PictureModel
}
export class PictureModel {
    large?: string = ""
    medium?: string = ""
    thumbnail?: string = "assets/img/avathar.jpg"
}
export class LocationModel {
    street?: string = "Street"
    city?: string = "city"
    state?: string = "state"
    zip?: string = "zip"
}
export class NameModel {
    title?: string
    firstName?: string
    lastName?: string
}