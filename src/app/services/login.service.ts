import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginModel } from '../models/login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseUrl = 'assets/json/login.json';

  constructor(private http: HttpClient) { 

  }
  getLoginList(): Observable<LoginModel[]> {
    return this.http.get<LoginModel[]>(this.baseUrl);
  }
}